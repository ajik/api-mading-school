import { Controller, Get, Post, Req , Body} from '@nestjs/common';
import { AppService } from './app.service';
import { Request } from 'express';
import { Message } from './model/message';
import common from './utils/common';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    getHello(): string {
        console.info('some text')
        return this.appService.getHello();
    }

    @Post('/board')
    async saveMessage(@Body() body: any) {
        try {
            console.info(body);
            const validCaptcha = await this.appService.verifyCaptcha(body.captchaToken)
            if(validCaptcha) {
                delete body.captchaToken;
                delete body.email;
                let data = await this.appService.insertMessage(body);
                return { status: 'success', message: '', data: data}
            } else {
                throw new Error('Invalid captcha!');
            }
        } catch (error) {
            return { status: 'error', message: error.message || error, data: null};
        }
    }

    @Get('/board/latest')
    async getLatestMessage(@Req() request: Request) {
        const { limit } = request.query;
        try {
            let data = await this.appService.getLatestMessages(Number(limit))
            return { status: 'success', message: '', data: data}
        } catch (error) {
            return { status: 'error', message: error.message || error, data: null}; 
        }
    }

    @Get('/board')
    async getMessages(@Req() request: Request) {
        const { limit = "10", page = "1" } = request.query;
        try {
            const data = await this.appService.getMessages(Number(limit), Number(page));
            return { status: 'success', message: '', data: data }
        } catch (error) {
            return { status: 'error', message: error.message || error, data: null}; 
        }
    }
}
