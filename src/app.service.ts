import { Injectable } from '@nestjs/common';
import { AppRepository } from './app.repository';
import { InjectConnection } from '@nestjs/mongoose';
import { mongo, Connection, Schema as MongooseSchema } from 'mongoose';
import { MessageDocument } from './schema/message.schema';
import { Message } from './model/message';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class AppService {

    constructor(
        @InjectConnection() private readonly mongoConnection: Connection, 
        private appRepository: AppRepository,
        private readonly httpService: HttpService
    ) {}
    
    getHello(): string {
        const txt = process.env.HELLO_WORLD_MESSAGE || 'Error'
        return txt;
    }

    async insertMessage(message: Message): Promise<MessageDocument> {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();

        try {
            const date = new Date();
            message.year = date.getFullYear().toString();
            message.month = (date.getMonth() + 1).toString();
            message.epochTime = date.getTime();
            
            const data = await this.appRepository.insertMessage(message, session);

            await session.commitTransaction();

            return data
        } catch (error) {
            console.error(`${error}`);
            await session.abortTransaction();
            throw new Error(error.message || error);
        } finally {
            session.endSession();
        }
    }

    async getLatestMessages(limit: number): Promise<MessageDocument[]> {
        const session = await this.mongoConnection.startSession();
        session.startTransaction();

        try {
            if(limit > 25) limit = 25;
            const data = await this.appRepository.getLatestMessages(limit, session);

            return data
        } catch (error) {
            console.error(`${error}`);
            await session.abortTransaction();
            throw new Error(error.message || error);
        } finally {
            session.endSession();
        }
    }

    async verifyCaptcha(captchaToken: string): Promise<boolean> {

        try {
            const response = await this.httpService.post(
                `${ process.env.RECAPTCHA_GOOGLE_URL || '' }`, 
                `secret=${ process.env.RECAPTCHA_SECRET_KEY || '' }&response=${ captchaToken }`,
                {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded" 
                    }
                }
            ).toPromise();
            const { data } = response;
            
            if(!data.success) {
                throw new Error(data['error-codes'][0] || 'Captcha Error!');

            } else {

                if(data?.action !== "submitNewMessage" || data?.action === 'NotFoundCaptcha') {
                    throw new Error('Captcha action is not correct!')
                }

                if (data?.score > 0.5) {
                    return true
                } else {
                    throw 'Google ReCaptcha Failure'
                }
            }
        } catch (error) {
            console.error(error);
            throw new Error(error.message || error);
        }
    }

    async getMessages(limit: number, page: number): Promise<any> {
        if(page < 1) page = 1;
        if(limit < 1) limit = 1;
        let offset = 0;
        let indexBeforeOffset = 0
        if(page > 1) {
            offset = limit * (page - 1);
            indexBeforeOffset = offset - 1;
        }

        const session = await this.mongoConnection.startSession();
        session.startTransaction();

        try {
            const totalData = await this.appRepository.getTotalMessages(session);
            const totalPage = Math.ceil(totalData/limit);
            const data = await this.appRepository.getMessagesWithLimitAndOffset(limit, offset, session);

            return { page, limit, totalData, totalPage, data }
        } catch (error) {
            console.error(`${error}`);
            await session.abortTransaction();
            throw new Error(error.message || error);
        } finally {
            session.endSession();
        }
    } 


}
