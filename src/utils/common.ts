export default {
    getRandomNumberFromRange: (min: number, max: number) => {
        return Math.floor(Math.random() * (max - min) ) + min;
    },
    corsOption: {
        origin: ['http://localhost:3000'],
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
        preflightContinue: false,
        allowedHeaders: "Content-Type, Accept, access_token, user_id, refresh_token",
        credentials: true
    }
}