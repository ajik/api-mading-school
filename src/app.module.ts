import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { MessageDocument, MessageSchema } from './schema/message.schema'
import { AppRepository } from './app.repository';
import { HttpModule } from '@nestjs/axios'

@Module({
    imports: [
        ConfigModule.forRoot(),
        MongooseModule.forRoot(`mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}/${process.env.MONGODB_DATABASE}?retryWrites=true&w=majority`),
        MongooseModule.forFeature([
            { name: MessageDocument.name, schema: MessageSchema }
        ]),
        HttpModule
    ],
    controllers: [AppController],
    providers: [AppService, AppRepository],
})
export class AppModule {}
