export type Message = {
    
    message: string;
    from: string;
    to: string;
    month: string;
    year: string;
    epochTime: number;
}