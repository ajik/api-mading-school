export type ResponseList<T> = {   
    limit: string;
    page: string;
    totalData: string;
    totalPage: string;
    dataList: T[];
}