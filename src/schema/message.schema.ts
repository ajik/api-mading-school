import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

// only for testing
@Schema({ collection: 'message' })
export class MessageDocument extends Document {

    @Prop({ type: String })
    message: string;

    @Prop({ type: String })
    from: string;

    @Prop({ type: String })
    to: string;

    @Prop({ type: String })
    month: string;

    @Prop({ type: String })
    year: string;

    @Prop({ type: Number })
    epochTime: number;

}

export const MessageSchema = SchemaFactory.createForClass(MessageDocument);