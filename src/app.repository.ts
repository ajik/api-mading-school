import { InjectModel } from '@nestjs/mongoose';
import { ClientSession, Model, Schema as MongooseSchema } from 'mongoose';
import { Message } from './model/message';
import { MessageDocument } from './schema/message.schema';

export class AppRepository {

    constructor(@InjectModel(MessageDocument.name) private readonly messageModel: Model<MessageDocument>) {}

    async insertMessage(data: Message, session: ClientSession): Promise<MessageDocument> {
        const message = await new this.messageModel(data)
        return await message.save({ session });
    }

    async getLatestMessages(limit: number = 10, session: ClientSession) {
        return await this.messageModel.find({}, null, { session }).sort({_id: -1}).limit(limit).exec();
    }

    async getTotalMessages(session: ClientSession) {
        return await this.messageModel.countDocuments(null, { session }).exec();
    }

    async getMessagesWithLimitAndOffset(limit: number, offset: number, session: ClientSession) {
        return await this.messageModel.aggregate([
            { $sort: { '_id': -1 }},
            { $skip: offset },
            { $limit: limit }
        ]).exec();
    }

    async getMessageBeforeOffset(indexBeforeOffset: number, session: ClientSession): Promise<MessageDocument[]> {
        return await this.messageModel.find({}, null, { session }).sort({ _id: -1 }).limit(indexBeforeOffset - 1)
    }

}
